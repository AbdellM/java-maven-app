def incrementVersion() {
    echo 'Increment the application version...'
    sh 'mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}

def buildJar() {
    echo 'building the application...'
    sh "mvn clean package"
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'nexus-repo', usernameVariable: 'REPO_USERNAME', passwordVariable: 'REPO_PWD')]){
        sh "docker build -t 172.17.0.3:8083/java-maven-app:$IMAGE_NAME ."
        sh "echo $REPO_PWD | docker login -u $REPO_USERNAME --password-stdin 172.17.0.3:8083"
        sh "docker push 172.17.0.3:8083/java-maven-app:$IMAGE_NAME"
    }} 

def deployApp() {
    echo 'deplying the application...'
}

def commitVersionUpdate() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-repo', usernameVariable: 'GITLAB_USERNAME', passwordVariable: 'GITLAB_PWD')]){
        sh 'git config --global user.email jenkins@jenkins.com'
        sh 'git config --global user.name jenkins'

        sh "git remote set-url origin https://${GITLAB_USERNAME}:${GITLAB_PWD}@gitlab.com/AbdellM/java-maven-app.git"
        sh 'git add pom.xml'
        sh 'git commit -m "CI: version bump"'
        sh 'git push origin HEAD:main'
    }
}
return this
